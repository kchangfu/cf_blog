layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer
        $ = layui.jquery;

    $(".loginBody .seraph").click(function(){
        layer.msg("这只是做个样式，至于功能，你见过哪个后台能这样登录的？还是老老实实的找管理员去注册吧",{
            time:5000
        });
    })

    //登录按钮
    form.on("submit(login)",function(data){
        $(this).text("登录中...");
        loginCheck($('#userName').val(), $('#password').val(), $('#code').val());
        return false;
        // // 登录ajax
        function loginCheck(username, password, code) {
            $.ajax({
                url: '/admin/login/loginCheck',
                type: 'post',
                data: { username: username, password: password, code: code },
                dataType: 'text',
                success: function (data) {
                    switch (data) {
                        case 'Verify_code_error':
                            layer.msg("验证码错误", { time: 3000 });
                            $('#login_sub').text("登录");
                            $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                            break;
                        case 'no_user':
                            layer.msg("非法操作，没有该用户名", {time: 3000});
                            $('#login_sub').text("登录");
                            break;
                        case 'no_admin':
                            layer.msg("该用户不是管理员", { time: 3000 });
                            $('#login_sub').text("登录");
                        case 'no_pass':
                            layer.msg("用户密码错误，请重新输入", { time: 3000 });
                            $('#login_sub').text("登录");
                            break;
                        case 'no_network':
                            layer.msg("网络错误...", { time: 3000 });
                            $('#login_sub').text("登录");
                            break;
                        case 'yes':
                            layer.msg("登录成功，正在跳转。。。", { time: 1000 });
                            setTimeout(function () {
                                window.location.href = "/admin";
                            }, 1000);
                            break;
                        // default:
                        //     setTimeout(function () {
                        //         window.location.href = "/admin/login";
                        //     }, 1000);
                        //     break;
                    }
                }
            });
        }
    })
    

    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    })
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    })
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
})
