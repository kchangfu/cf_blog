layui.use(['form','layer','laydate','table','upload'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        upload = layui.upload,
        table = layui.table;

    //友链列表
    var tableIns = table.render({
        elem : '#linkList',
        // url : '/static/admin/json/linkList.json',
        url : '/admin/Systemsetting/GetLinks',
        method : 'POST',
        page : true,
        cellMinWidth : 95,
        height : "full-104",
        limit : 5,
        limits : [5,10,15,20],
        id : "linkListTab",
        cols : [[
            {field: 'id', type: "checkbox", fixed: "left", width: 50 },
            {field: 'logo', title: 'LOGO', width:180, align:"center",templet:function(d){
                return '<a href="'+d.url+'" target="_blank"><img src="'+d.logo+'" height="26" class="LogoImg'+d.id+'" /></a>';
            }},
            {field: 'name', title: '网站名称', minWidth:240},
            {field: 'url', title: '网站地址',width:300,templet:function(d){
                return '<a class="layui-blue" href="'+d.url+'" target="_blank">'+d.url+'</a>';
            }},
            {field: 'masterEmail', title: '站长邮箱',minWidth:200, align:'center'},
            {field: 'display', title: '展示位置', align:'center',templet:function(d){
                return d.display == "1" ? "显示" : "隐藏";
            }},
            {field: 'update_time', title: '更新时间', align:'center',minWidth:110},
            {title: '操作', width:130,fixed:"right",align:"center", templet:function(){
                return  '<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>'
                       +'<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>';
            }}
        ]]
    });

    //搜索功能
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("linkListTab",{
                url: '/admin/Systemsetting/searchLink',
                method: 'POST',
                where: {
                    key: $(".searchVal").val()
                }
            })
        }else{
            tableIns.reload();  //重载
            // layer.msg("请输入搜索的内容");
        }
    });
    //添加友链
    function addLink(edit){
        var index = layer.open({
            title : "添加友链",
            type : 2,
            area : ["300px","385px"],
            content : "admin/systemSetting/linksAdd",
            success : function(layero, index){
                var body = $($(".layui-layer-iframe",parent.document).find("iframe")[0].contentWindow.document.body);
                if(edit){
                    body.find(".linkLogo").css("background","#fff");
                    body.find(".linkLogoImg").attr("src",edit.logo);
                    body.find(".linkName").val(edit.name);
                    body.find(".linkUrl").val(edit.url);
                    body.find(".masterEmail").val(edit.masterEmail);
                    body.find(".display_s").prop("checked", edit.display);
                    body.find(".linkId").val(edit.id);
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回友链列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }
    $(".addLink_btn").click(function(){
        addLink();
    })
    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('linkListTab'),
            data = checkStatus.data,
            linkId = [];
        if(data.length > 0) {
            for (var i in data) {
                linkId.push(data[i].id);
            }
            layer.confirm('确定删除选中的友链？', {icon: 3, title: '提示信息'}, function (index) {
                $.post("DelLink",{
                    id : linkId  //将需要删除的linkId作为参数传入
                },function(data){
                    if (data == 200) layerMsg(index, '批量删除成功！', 0);
                    else layerMsg(index, '批量删除失败！', 500);
                })
            })
        }else{
            layer.msg("请选择需要删除的友链");
        }
    })

    //列表操作-更新或删除
    table.on('tool(linkList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
            addLink(data);
        } else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此友链？',{icon:3, title:'提示信息'},function(index){
                $.post("DelLink",{
                    id : data.id  //将需要删除的linkId作为参数传入
                },function(data){
                    if (data == 200) layerMsg(index,'删除成功！',0);
                    else layerMsg(index, '删除失败！', 500);
                })
            });
        }
    });
    //上传logo
    upload.render({
        elem: '.linkLogo',
        url: 'uploadsLogo',
        data: { id: $('.linkId').val(), whoLogo:'linkLogo'},
        done: function(res){
            var index = top.layer.msg('上传中。。。', { icon: 16, time: false, shade: 0.8 });
            if (res.code == 200){
                layerMsg(index, '上传成功！', 500);
            }
        }
    });

    //处理时间
    // var time = new Date();
    //格式化时间
    // function filterTime(val){
        // if(val < 10){
        //     return "0" + val;
        // }else{
        //     return val;
        // }
    // }
    // var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());
    
    // 提交按钮
    form.on("submit(addLink)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        // 实际使用时的提交信息
        // $.post("上传路径",{
        //     linkLogoImg : $(".linkLogo").attr("src"),  //logo
        //     linkName : $(".linkName").val(),  //网站名称
        //     linkUrl : $(".linkUrl").val(),    //网址
        //     masterEmail : $('.masterEmail').val(),    //站长邮箱
        //     showAddress : data.filed.showAddress == "on" ? "checked" : "",    //展示位置
        //     newsTime : submitTime,    //发布时间
        // },function(res){
        //
        // })
        $.ajax({
            url: 'AddLink',
            type:'post',
            data: {
                id: $(".linkId").val(),
                logo: $(".linkLogo").attr("src"),
                name: $(".linkName").val(),
                url: $(".linkUrl").val(),
                masterEmail: $('.masterEmail').val(),
                display: $(".display").val()
            },
            dataType: "json",
            success: function (res) {
                if (res == 200) layerMsg(index,'添加成功！',500);
                else if (res == 201) layerMsg(index, '更新成功！', 500);
                else if (res == 500) layerMsg(index, '失败!', 500);
            },
            error: function (err) {
                layerMsg(index, '网络错误:'+err, 500);
            }
        })
        return false;
    })
    // 监听 是否显示开关
    form.on('switch', function (data) {
        $(data.othis).after('<input type="hidden" class="display" value=' + (data.elem.checked ? '1' : '0') + '>');
    });

    // 定时器，提示
    function layerMsg(index, msg, time) {
        setTimeout(function () {
            layer.close(index);
            layer.msg(msg);
            layer.closeAll("iframe");
            //刷新父页面
            $(".layui-tab-item.layui-show", parent.document).find("iframe")[0].contentWindow.location.reload();
        }, time);
    }
})