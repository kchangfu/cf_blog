layui.use(['form', 'layer', 'upload','layedit'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        upload = layui.upload,
        layedit = layui.layedit
        about_author = layedit.build('about_author');

    //上传web_logo
    upload.render({
        elem: '.author_pic',
        url: 'uploadsLogo',
        data: { whoLogo: 'author_Pic'},
        done: function(res){
            var index = top.layer.msg('上传中。。。', { icon: 16, time: false, shade: 0.8 });
            if (res.code == 200){
                layerMsg(index, '上传成功！', 500);
            }
        }
    });

    // 提交按钮
    form.on("submit(about_author)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        $.ajax({
            url: 'about_author',
            type:'post',
            data: {
                author_name: $(".author_name").val(),
                author_desc: $(".author_desc").val(),
                author_addr: $(".author_addr").val(),
                about_author: layedit.getContent(about_author),
            },
            dataType: "json",
            success: function (res) {
                if (res == 200) layerMsg(index,'更新成功！',500);
                else if (res == 400) layerMsg(index, '更新失败!', 500);
            },
            error: function (err) {
                layerMsg(index, '网络错误:'+err, 500);
            }
        })
        return false;
    })
    // 监听 是否显示开关
    form.on('switch', function (data) {
        $(data.othis).after('<input type="hidden" class="display" value=' + (data.elem.checked ? '1' : '0') + '>');
    });

    // 定时器，提示
    function layerMsg(index, msg, time) {
        setTimeout(function () {
            layer.close(index);
            layer.msg(msg);
            layer.closeAll("iframe");
            //刷新父页面
            $(".layui-tab-item.layui-show", parent.document).find("iframe")[0].contentWindow.location.reload();
        }, time);
    }
})