layui.use(['form','layer','jquery'],function(){
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		laypage = layui.laypage,
		$ = layui.jquery;

 	form.on("submit(systemParameter)",function(data){
		let systemdata = {
			"web_name": $(".web_name").val(),				//网站名称
			"web_version": $(".web_version").val(),			//网站当前版本
			"web_url": $(".web_url").val(),				//网站url链接
			"web_copy": $(".web_copy").val(),				//网站版权信息
			"web_icp": $(".web_icp").val(),					//网站ICP备案
			"web_description": $(".web_description").val(), //网站描述
			"web_keywords": $(".web_keywords").val()		//网站关键字
		}
		var index = top.layer.msg('数据提交中，请稍候', { icon: 16, time: false, shade: 0.8 });
		$.ajax({
			url: "/admin/Systemsetting/basicParameter",
			type : "post",
			data: systemdata,
			dataType : "json",
			success: function(data){
				if (data == 200){
					layerMsg(index, '系统基本参数修改成功！' + data, 500);
					fillData(data);
				} else if (data == 400) {
					layerMsg(index, '您没有做任何改变！' + data, 500);
				}
			},
			error: function(err){
				layerMsg(index, '修改失败！'+err, 500);
			}
		});
		return false;
 	})


	 //加载默认数据
 	// if(window.sessionStorage.getItem("systemParameter")){
 	// 	var data = JSON.parse(window.sessionStorage.getItem("systemParameter"));
 	// 	fillData(data);
 	// }else{
 	// 	$.ajax({
	// 		url : "http://b.cn/static/admin/json/systemParameter.json",
	// 		type : "get",
	// 		dataType : "json",
	// 		success : function(data){
	// 			fillData(data);
	// 		}
	// 	})
	 // }
	
	// 定时器，提示
	function layerMsg(index,msg,time) {
		setTimeout(function () {
			layer.close(index);
			layer.msg(msg);
		}, time);
	}
 	//填充数据方法
 	function fillData(data){
 		$(".version").val(data.version);      //当前版本
		$(".author").val(data.author);        //开发作者
		$(".homePage").val(data.homePage);    //网站首页
		$(".server").val(data.server);        //服务器环境
		$(".dataBase").val(data.dataBase);    //数据库版本
		$(".maxUpload").val(data.maxUpload);  //最大上传限制
		$(".userRights").val(data.userRights);//当前用户权限
		$(".cmsName").val(data.cmsName);      //模版名称
		$(".description").val(data.description);//站点描述
		$(".powerby").val(data.powerby);      //版权信息
		$(".record").val(data.record);      //网站备案号
		$(".keywords").val(data.keywords);    //默认关键字
 	}
 	
})
