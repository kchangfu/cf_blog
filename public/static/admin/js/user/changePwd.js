layui.use(['form','layer','jquery'],function(){
    var form  = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $     = layui.jquery;

    //添加验证规则
    form.verify({
        oldPwd : function(value, item){
            if(value != "123456"){
                return "密码错误，请重新输入！";
            }
        },
        newPwd : function(value, item){
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        },
        confirmPwd : function(value, item){
            if(!new RegExp($("#NewPwd").val()).test(value)){
                return "两次输入密码不一致，请重新输入！";
            }
        }
    })

    form.on("submit(changePwd)",function(){
        var data = {
            username:$('#username').val(),
            oldPwd:$('#OldPwd').val(),
            newPwd:$("#NewPwd").val()
        }
        $.ajax({
            url: 'changePwd',
            type: 'post',
            data: data,
            dataType: "text",
            success: function (res) {
                switch (res) {
                    case 'oldPwd error':
                        layer.msg("旧密码错误...", { time: 3000 });
                        break;
                    case 'changePwd success':
                        layer.msg("修改密码成功...", { time: 3000 });
                        break;
                }
            },
            error: function (err) {
                layer.msg('网络错误...'+err, { time: 3000 });
            }
        })
    })
})