/*

@Name：不落阁整站模板源码 
@Author：Absolutely 
@Site：http://www.lyblogs.cn

*/

layui.use(['form','layer','jquery'],function(){
    var form  = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $     = layui.jquery;
    //添加验证规则
    form.verify({
        confirmPwd : function(value, item){
            if(value != $("#LAY-user-register-password").val()){
                return "两次输入密码不一致，请重新输入！";
            }
        }
    })
    // 注册提交
    form.on("submit(LAY-user-register-submit)",function(){
        var data = {
            username:$('#LAY-user-register-username').val(),
            email:$('#LAY-user-register-email').val(),
            password:$("#LAY-user-register-password").val(),
            code:$("#LAY-user-register-vercode").val(),
        }
        $.ajax({
            url: 'register',
            type: 'post',
            data: data,
            dataType: "text",
            success: function (res) {
                switch (res) {
                    case '400_username':
                        layer.msg("该用户名已经存在...", { time: 3000 });
                        $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                        break;
                    case '400_email':
                        layer.msg("该邮箱已被注册...", {time: 3000});
                        $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                        break;
                    case '400_code':
                        layer.msg("验证码错误...", { time: 3000 });
                        $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                        break;
                    case '200_success':
                        layer.msg("注册成功，已为您自动登陆...", { time: 3000 });
                        window.location.href = '/index/home';
                        break;
                    case '500_error':
                        layer.msg("注册失败，请用qq登陆...", { time: 3000 });
                        $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                        break;
                }
            },
            error: function (err) {
                layer.msg('注册失败，请用qq登陆...'+err, { time: 3000 });
                $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
            }
        })
    })

    // 登录提交
    form.on("submit(LAY-user-login-submit)",function(data){
        $(this).text("登录中...");
        loginCheck($('#LAY-user-login-username').val(), $('#LAY-user-login-password').val(), $('#LAY-user-login-vercode').val());
        return false;
        // // 登录ajax
        function loginCheck(username, password, code) {
            $.ajax({
                url: 'login',
                type: 'post',
                data: { username: username, password: password, code: code },
                dataType: 'text',
                success: function (data) {
                    switch (data) {
                        case 'Verify_code_error':
                            layer.msg("验证码错误", { time: 3000 });
                            $('#LAY-user-login-submit').text("登 录");
                            $('#captcha_src').attr('src', '/captcha?id=' + Date.parse(new Date) / 1000);
                            break;
                        case 'no_user':
                            layer.msg("非法操作，没有该用户名", {time: 3000});
                            $('#LAY-user-login-submit').text("登 录");
                            break;
                        case 'no_pass':
                            layer.msg("用户密码错误，请重新输入", { time: 3000 });
                            $('#LAY-user-login-submit').text("登 录");
                            break;
                        case 'no_network':
                            layer.msg("网络错误...", { time: 3000 });
                            $('#LAY-user-login-submit').text("登 录");
                            break;
                        case 'yes':
                            layer.msg("登录成功", { time: 1000 });
                            setTimeout(function () {
                                window.location.href = "/index/home";
                            }, 1000);
                            break;
                    }
                }
            });
        }
    })
})