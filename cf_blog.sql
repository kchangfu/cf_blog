-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2018-05-12 13:42:01
-- 服务器版本： 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cf_blog`
--

-- --------------------------------------------------------

--
-- 表的结构 `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `web_name` varchar(32) NOT NULL,
  `web_url` varchar(100) NOT NULL,
  `web_logo` varchar(255) DEFAULT '/static/index/images/Logo_100.png',
  `web_copy` varchar(32) NOT NULL,
  `web_icp` varchar(32) NOT NULL,
  `web_version` varchar(8) NOT NULL,
  `web_close` int(1) NOT NULL DEFAULT '1',
  `web_description` varchar(255) DEFAULT NULL,
  `web_keywords` varchar(255) DEFAULT NULL,
  `web_synopsis` text,
  `Update_ByIP` varchar(45) DEFAULT NULL,
  `Last_Update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `about_blog` text,
  `about_author` text,
  `author_name` varchar(50) DEFAULT NULL,
  `author_pic` varchar(255) DEFAULT NULL,
  `author_addr` varchar(50) DEFAULT NULL,
  `author_desc` text,
  `author_info` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `config`
--

INSERT INTO `config` (`id`, `web_name`, `web_url`, `web_logo`, `web_copy`, `web_icp`, `web_version`, `web_close`, `web_description`, `web_keywords`, `web_synopsis`, `Update_ByIP`, `Last_Update`, `about_blog`, `about_author`, `author_name`, `author_pic`, `author_addr`, `author_desc`, `author_info`) VALUES
(1, '阿长的博客', 'http://kchangfu.cn', '/uploads/web_Logo/20180303/efcc73b3827c1764501d6e77acb19a3d.png', 'Copyright © 2018', '闽ICP备17011862号-1', 'v-3', 1, '记录博主学习和成长之路，分享web开发方面技术和源码', 'layui，CMS，博客，BLOG，kcf，cf，thinkPHP，Tp5', ' <p style="text-align:center;">不落阁是一个由ASP.NET MVC开发的个人博客网站，诞生于2016年11月7日，起劲为止经历了一次大改，暂且称为不落阁2.0。</p><h1>第一个版本</h1><p>诞生的版本，采用ASP.NET MVC + Entity Framework作为后台框架，前端几乎自己手写，用了Bootstrap的栅格系统来布局！起初并没有注意美工，只打算完成基本的功能，故视觉体验是比较差的。</p><h1>第二个版本</h1><p>由于感觉EF查询数据的时候较慢（后来发现是自己搞错了），于是自己写了个ORM，其实也算不上ORM，就是将ADO.NET进行封装，再封装，再利用反射将数据库表与实体类一一对应，有了基本的增删改查、事务、自动建表等功能，同时为了配合这个ORM，将项目改成三层，前端方面加入了Animate.css的动画效果，同时自己手写了几个动画，并制作了浅色于深色两种主题的样式，视觉体验稍有提高。</p><h1>当前版本</h1><p>从公司的一个后台管理系统的前端发现了Layer弹窗插件，于是追根溯源，发现了Layui前端框架！Layui简洁的风格让我很是喜欢，于是决定再次将网站改版！此次改版从里到外几乎全部更新。后台增加了面向接口开发，使用了IOC框架，同时ORM回归到Entity Framework，前端则移除Bootstarp，引入Layui。视觉体验显著提高。</p><h1 style="text-align:center;">The End</h1>', '::1', '2018-05-01 22:53:51', ' <p style="text-align:center;">骄傲的采用了ThinkPHP5和前端UI框架，大部分功能还在开发当中。</p><p style="text-align:center;"><br></p><h1 style="text-align:center;">The End</h1>', '<div class="layui-field-box aboutinfo-abstract abstract-bloger">\n                                        <p style="text-align: center;"></p><p style="text-align: center;"><br></p><p></p>\n                                        <h1>个人信息</h1>\n                                        <p><!--5f39ae17-8c62-4a45-bc43-b32064c9388a:W3siYmxvY2tUeXBlIjoicGFyYWdyYXBoIiwic3R5bGVzIjp7ImxpbmUtaGVpZ2h0IjoxLjc4NTcxNDI4NTcxNDI4NTgsImFsaWduIjoibGVmdCIsImluZGVudCI6MCwidGV4dC1pbmRlbnQiOjB9LCJibG9ja0lkIjoiMjUxMS0xNTE4MjQzNDE4MDczIiwicmljaFRleHQiOnsiaXNSaWNoVGV4dCI6dHJ1ZSwia2VlcExpbmVCcmVhayI6dHJ1ZSwiZGF0YSI6W3siY2hhciI6IuWNmiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5Li7Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLnvZEiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWQjSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi4oCUIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJLIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJlIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJyIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJyIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLkuIAiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuS4qiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5ZCNIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiI5Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiI1Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLlkI4iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IueahCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi56iLIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLluo8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IueMvyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiLCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi6K6hIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLnrpciLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuacuiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5q+VIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLkuJoiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Iu+8jCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi546wIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLku44iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuS6iyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiICIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi55qEIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmmK8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Iue9kSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi56uZIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJ3Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJlIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiJiIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLlvIAiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWPkSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi44CCIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmiYsiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuacuiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5o6nIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLvvIwiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWWnCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5qyiIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmjaMiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Ium8kyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5pawIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLpspwiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IueOqSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5oSPIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLlhL8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuOAgiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5LiaIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLkvZkiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IueIsSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5aW9Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmmK8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IueOqSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5ri4Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmiI8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Iu+8jCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi6LeRIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmraUiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Iu+8jCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5peFIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmuLgiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuOAgiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5ZacIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmrKIiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuS6pCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5pawIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmnIsiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWPiyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi44CCIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fV19fV0=--></p><p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<b>座右铭</b>：</p><p><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;迷茫是因为能力还配不上梦想！</span></p><p><span><br></span></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>联系方式</b>：187********</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>QQ</b> :187********<br></p><p></p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>微信号</b> :187********</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>邮箱</b> : 187********@163.com</p><p></p><p></p><p></p><!--5f39ae17-8c62-4a45-bc43-b32064c9388a:W3siYmxvY2tUeXBlIjoicGFyYWdyYXBoIiwic3R5bGVzIjp7ImFsaWduIjoibGVmdCIsImluZGVudCI6MCwidGV4dC1pbmRlbnQiOjAsImxpbmUtaGVpZ2h0IjoxLjc1fSwiYmxvY2tJZCI6IjQ4NjMtMTUxODI0MzQxODA3MyIsInJpY2hUZXh0Ijp7ImlzUmljaFRleHQiOnRydWUsImtlZXBMaW5lQnJlYWsiOnRydWUsImRhdGEiOlt7ImNoYXIiOiLluqciLCJzdHlsZXMiOnsiYm9sZCI6dHJ1ZSwiZm9udC1mYW1pbHkiOiJBcmlhbCIsImZvbnQtc2l6ZSI6MTYsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWPsyIsInN0eWxlcyI6eyJib2xkIjp0cnVlLCJmb250LWZhbWlseSI6IkFyaWFsIiwiZm9udC1zaXplIjoxNiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi6ZOtIiwic3R5bGVzIjp7ImJvbGQiOnRydWUsImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJmb250LXNpemUiOjE2LCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLvvJoiLCJzdHlsZXMiOnsiYm9sZCI6dHJ1ZSwiZm9udC1mYW1pbHkiOiJBcmlhbCIsImZvbnQtc2l6ZSI6MTYsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19XX19LHsiYmxvY2tUeXBlIjoicGFyYWdyYXBoIiwic3R5bGVzIjp7ImxpbmUtaGVpZ2h0IjoxLjc4NTcxNDI4NTcxNDI4NTgsImFsaWduIjoibGVmdCIsImluZGVudCI6MCwidGV4dC1pbmRlbnQiOjB9LCJibG9ja0lkIjoiMzIyMC0xNTE4MjQzNDE4MDczIiwicmljaFRleHQiOnsiaXNSaWNoVGV4dCI6dHJ1ZSwia2VlcExpbmVCcmVhayI6dHJ1ZSwiZGF0YSI6W3siY2hhciI6Iui/tyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi6IyrIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmmK8iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWboCIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5Li6Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLog70iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWKmyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi6L+YIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLphY0iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuS4jSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5LiKIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmoqYiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuaDsyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi77yBIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fV19fSx7ImJsb2NrVHlwZSI6InBhcmFncmFwaCIsInN0eWxlcyI6eyJsaW5lLWhlaWdodCI6MS43ODU3MTQyODU3MTQyODU4LCJhbGlnbiI6ImxlZnQiLCJpbmRlbnQiOjAsInRleHQtaW5kZW50IjowfSwiYmxvY2tJZCI6IjcwOTgtMTUxODI0MzQxODA3MyIsInJpY2hUZXh0Ijp7ImlzUmljaFRleHQiOnRydWUsImtlZXBMaW5lQnJlYWsiOnRydWUsImRhdGEiOlt7ImNoYXIiOiLogZQiLCJzdHlsZXMiOnsiYm9sZCI6dHJ1ZSwiZm9udC1mYW1pbHkiOiJBcmlhbCIsImZvbnQtc2l6ZSI6MTYsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuezuyIsInN0eWxlcyI6eyJib2xkIjp0cnVlLCJmb250LWZhbWlseSI6IkFyaWFsIiwiZm9udC1zaXplIjoxNiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5pa5Iiwic3R5bGVzIjp7ImJvbGQiOnRydWUsImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJmb250LXNpemUiOjE2LCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLlvI8iLCJzdHlsZXMiOnsiYm9sZCI6dHJ1ZSwiZm9udC1mYW1pbHkiOiJBcmlhbCIsImZvbnQtc2l6ZSI6MTYsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6Iu+8miIsInN0eWxlcyI6eyJib2xkIjp0cnVlLCJmb250LWZhbWlseSI6IkFyaWFsIiwiZm9udC1zaXplIjoxNiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX1dfX0seyJibG9ja1R5cGUiOiJwYXJhZ3JhcGgiLCJzdHlsZXMiOnsibGluZS1oZWlnaHQiOjEuNzg1NzE0Mjg1NzE0Mjg1OCwiYWxpZ24iOiJsZWZ0IiwiaW5kZW50IjowLCJ0ZXh0LWluZGVudCI6MH0sImJsb2NrSWQiOiI1ODE0LTE1MTgyNDM0MTgwNzMiLCJyaWNoVGV4dCI6eyJpc1JpY2hUZXh0Ijp0cnVlLCJrZWVwTGluZUJyZWFrIjp0cnVlLCJkYXRhIjpbeyJjaGFyIjoiUSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiUSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiICIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiOiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX1dfX0seyJibG9ja1R5cGUiOiJwYXJhZ3JhcGgiLCJzdHlsZXMiOnsibGluZS1oZWlnaHQiOjEuNzg1NzE0Mjg1NzE0Mjg1OCwiYWxpZ24iOiJsZWZ0IiwiaW5kZW50IjowLCJ0ZXh0LWluZGVudCI6MH0sImJsb2NrSWQiOiIyNzgyLTE1MTgyNDM0MTgwNzMiLCJyaWNoVGV4dCI6eyJpc1JpY2hUZXh0Ijp0cnVlLCJrZWVwTGluZUJyZWFrIjp0cnVlLCJkYXRhIjpbeyJjaGFyIjoi5b6uIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLkv6EiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWPtyIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiICIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiOiIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoiICIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX1dfX0seyJibG9ja1R5cGUiOiJwYXJhZ3JhcGgiLCJzdHlsZXMiOnsibGluZS1oZWlnaHQiOjEuNzg1NzE0Mjg1NzE0Mjg1OCwiYWxpZ24iOiJsZWZ0IiwiaW5kZW50IjowLCJ0ZXh0LWluZGVudCI6MH0sImJsb2NrSWQiOiI1MDIxLTE1MTgyNDM0MTgwNzMiLCJyaWNoVGV4dCI6eyJpc1JpY2hUZXh0Ijp0cnVlLCJrZWVwTGluZUJyZWFrIjp0cnVlLCJkYXRhIjpbeyJjaGFyIjoi6YKuIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLnrrEiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IiAiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IjoiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IiAiLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IumCriIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5Lu2Iiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fSx7ImNoYXIiOiLmnI0iLCJzdHlsZXMiOnsiZm9udC1mYW1pbHkiOiJBcmlhbCIsImNvbG9yIjoiIzU4NjY2ZSIsImJhY2stY29sb3IiOiIjZmZmYmU1In19LHsiY2hhciI6IuWKoSIsInN0eWxlcyI6eyJmb250LWZhbWlseSI6IkFyaWFsIiwiY29sb3IiOiIjNTg2NjZlIiwiYmFjay1jb2xvciI6IiNmZmZiZTUifX0seyJjaGFyIjoi5ZmoIiwic3R5bGVzIjp7ImZvbnQtZmFtaWx5IjoiQXJpYWwiLCJjb2xvciI6IiM1ODY2NmUiLCJiYWNrLWNvbG9yIjoiI2ZmZmJlNSJ9fV19fV0=-->\n                                        <h1>个人介绍</h1>\n                                        <p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="text-align: center;">kchangfu，</span>手机控，喜欢捣鼓新鲜玩意儿。业余爱好是玩游戏，旅游。</p>\n                                        <h1 style="text-align: center;">The End</h1>\n                                    </div>', 'kchangfu', '/uploads/author_Pic/20180303/bb6cd83f71ff0a647913ef92e8e17999.gif', '广东-深圳', '主攻B/S架构，略懂Web前端', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `gbook`
--

CREATE TABLE `gbook` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `url` varchar(255) NOT NULL,
  `masterEmail` varchar(50) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `rank` tinyint(4) NOT NULL DEFAULT '1',
  `display` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `links`
--

INSERT INTO `links` (`id`, `name`, `url`, `masterEmail`, `desc`, `logo`, `rank`, `display`, `create_time`, `update_time`) VALUES
(1, 'layui', 'http://www.layui.com', 'xianxin@layui.com', 'layui.com', '/static/admin/images/layui.png', 1, 1, '2018-02-04 21:54:55', '2018-02-04 21:54:55'),
(4, 'ThinkPHP', 'http://www.thinkphp.cn/', 'marketing@topthink.net', 'ThinkPHP框架', '/static/admin/images/mayun.png', 2, 1, '2018-02-04 21:54:55', '2018-02-04 21:54:55'),
(6, 'layui', 'http://www.layui.com', 'xianxin@layui.com', 'layui.com', '/uploads/linkLogo/20180429/5b6f37716849b1d908a1e6d52c3a34c8.jpg', 6, 0, '2018-02-04 21:54:55', '2018-02-04 21:54:55');

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `login_type` tinyint(1) NOT NULL DEFAULT '0',
  `userStatus` tinyint(1) NOT NULL DEFAULT '1',
  `prev_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logo` varchar(255) DEFAULT '/static/index/images/user_ico.jpg',
  `Name` varchar(32) DEFAULT NULL,
  `Sex` varchar(1) DEFAULT NULL,
  `Birth_Date` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `synopsis` varchar(200) DEFAULT NULL,
  `Phone` varchar(11) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `QQ` int(11) DEFAULT NULL,
  `WeChat` varchar(50) DEFAULT NULL,
  `WeiBo` varchar(50) DEFAULT NULL,
  `Git` varchar(50) DEFAULT NULL,
  `skills` text,
  `evaluation` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `login_type`, `userStatus`, `prev_login`, `last_login`, `logo`, `Name`, `Sex`, `Birth_Date`, `address`, `synopsis`, `Phone`, `Email`, `QQ`, `WeChat`, `WeiBo`, `Git`, `skills`, `evaluation`) VALUES
(1, 'admin', 'B8W9xVTLh4/2vuNGZoZafRG2fMNKg5QJ6Bn3mnOZmrw=', 2, 1, '2018-05-12 19:25:27', '2018-05-12 20:51:43', '/uploads/author_Pic/20180303/bb6cd83f71ff0a647913ef92e8e17999.gif', 'kchangfu', 'M', '1995-07-16 05:00:00', '福建/泉州/惠安', '一枚90后程序员，PHP开发工程师，主攻B/S架构，略懂Web前端', '1875992974*', '281913651@qq.com', 281913651, NULL, NULL, NULL, NULL, '<div class="layui-field-box aboutinfo-abstract abstract-bloger">\n                                        <p style="text-align:center;">Absolutely，不落阁创始人，诞生于1996年2月14日，目前是一个码农，从事.NET开发。</p>\n                                        <h1>个人信息</h1>\n                                        <p>暂无</p>\n                                        <h1>个人介绍</h1>\n                                        <p>一个没有故事的男同学，没什么介绍......</p>\n                                        <h1 style="text-align:center;">The End</h1>\n                                    </div>'),
(12, 'kcf', 'B8W9xVTLh4/2vuNGZoZafRG2fMNKg5QJ6Bn3mnOZmrw=', 0, 1, '2018-05-12 20:48:01', '2018-05-12 20:52:15', '/static/index/images/user_ico.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '1015747345@qq.com', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gbook`
--
ALTER TABLE `gbook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `gbook`
--
ALTER TABLE `gbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
