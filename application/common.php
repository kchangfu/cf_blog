<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件


/**
 *  上传文件函数，
 *  参数：$file ,$path
 *  返回：成功=>code(200), SaveName;
 *  返回: 失败=>code(300),;
 */
function FileUploads($file, $path){
    if($file->getError()) return ['code'=>300, 'data'=>'', 'msg'=>'上传文件失败！'];
    else{
        $info = $file->move($path);
        if (!$info) return ['code'=>300, 'data'=>'', 'msg'=>'移动文件失败！'];
        else {
            // 成功上传， 获取保存文件名
            $SaveNameT = $info->getSaveName();
            $SaveName = str_replace("\\","/",$SaveNameT);
            return ['code'=>200, 'data'=>$SaveName, 'msg'=>'上传文件成功！'];
        }
    }
}
/**
 *  格式化用户权限
 *  参数： $login_type 权限代码
 *  返回： 游客/普通管理员/超级管理员
 */
function formatuserRights($login_type){
    switch($login_type){
        case '0':
            return '游客';
            break;
        case '1':
            return '管理员';
            break;
        case '2':
            return '超级管理员';
            break;
    }
}
/**
 *  格式化用户性别
 *  参数： $Sex 用户性别代码 M男F女
 *  返回： 男/女/保密
 */
function formatuserSex($Sex){
    switch($Sex){
        case 'M':
            return '男';
            break;
        case 'F':
            return '女';
            break;
        default:
            return '保密';
            break;
    }
}
/**
 *  PHP加密和解密函数
 *  可以用来加密一些有用的字符串存放在数据库里，并且通过可逆解密字符串，该函数使用了base64和MD5加密和解密。
 *  参数： $string加密字符串，$decrypt（0加密1解密）
 *  返回： 男/女/保密
 *  例子： 以下是将字符串“Helloweba欢迎您”分别加密和解密
 *      加密：echo encryptDecrypt('password', 'Helloweba欢迎您',0);
 *      解密：echo encryptDecrypt('password''z0JAx4qMwcF+db5TNbp/xwdUM84snRsXvvpXuaCa4Bk='1);
 */
function encryptDecrypt($string, $decrypt){
    $key = 'kchangfu';
    if($decrypt){
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "12");
        return $decrypted;
    }else{
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
        return $encrypted;
    }
}
    