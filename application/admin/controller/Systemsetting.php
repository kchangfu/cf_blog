<?php
namespace app\admin\controller;
use think\Controller;	//引入Controller类
use app\admin\model\Config as ConfigModel;
use app\admin\model\Links as LinkModel;
use app\common;
/**
 * 系统设置
 */
class Systemsetting extends Controller
{
    #####################基本参数########################
    public function basicParameter(){
        $ConfigModel = new ConfigModel;
        if ($_POST) {
            $_POST['Last_Update'] = date('Y-m-d H:i:s');
            $_POST['Update_ByIP'] = $_SERVER['REMOTE_ADDR'];
            $resule = $ConfigModel->saveConfig(1,$_POST);
            if($resule) echo 200;
            else echo 400;
            die;
        }
        $config = $ConfigModel->getConfig(1);
        $this->assign('config',$config);
        return $this->fetch();
    }
    #####################关于博客########################
    public function about_blog(){
        $ConfigModel = new ConfigModel;
        if ($_POST) {
            $_POST['Last_Update'] = date('Y-m-d H:i:s');
            $_POST['Update_ByIP'] = $_SERVER['REMOTE_ADDR'];
            $resule = $ConfigModel->saveConfig(1,$_POST);
            if($resule) echo 200;
            else echo 400;
            die;
        }
        $config = $ConfigModel->getConfig(1);
        $this->assign('config',$config);
        return $this->fetch();
    }
    #####################关于博主########################
    public function about_author(){
        $ConfigModel = new ConfigModel;
        if ($_POST) {
            $_POST['Last_Update'] = date('Y-m-d H:i:s');
            $_POST['Update_ByIP'] = $_SERVER['REMOTE_ADDR'];
            $resule = $ConfigModel->saveConfig(1,$_POST);
            if($resule) echo 200;
            else echo 400;
            die;
        }
        $config = $ConfigModel->getConfig(1);
        $this->assign('config',$config);
        return $this->fetch();
    }
    #####################友情链接########################
    // 网站友情链接页面
    public function linkList(){
        return $this->fetch();
    }
    // 添加或者编辑友链-页面
    public function linksAdd(){
        return $this->fetch();
    }
    // ajax获取友情链接
    public function GetLinks(){
        if($_POST['page'] && $_POST['limit']){
            $limit = $_POST['limit'];
            $page = $_POST['page'];
            // $offset = $_POST['limit'] * ($_POST['page'] - 1);
            $LinkModel = new LinkModel();
            $links = $LinkModel->getLinks($limit,$page);
            $resule = (object)[];
            $resule->code = 0;
            $resule->msg = '';
            $resule->count = count($LinkModel->getLinksCount());
            $resule->data = [];
            foreach ($links as $value) {
                array_push($resule->data,$value);
            }
            echo json_encode($resule);
        }
    }
    // ajax添加友情链接
    public function AddLink(){
        $LinkModel = new LinkModel();
        if($_POST['id'] != ''){
            $resule = $LinkModel->upd_Link($_POST['id'],$_POST);
            if($resule == 500) echo 500;
            else echo 201;
        }else {
            $resule = $LinkModel->ins_Link($_POST);
            if($resule == 500) echo 500;
            else echo 200;
        }
    }
    // ajax删除友情链接
    public function DelLink(){
        $LinkModel = new LinkModel();
        if(!is_array($_POST['id'])){
            $resule = $LinkModel->del_Link($_POST['id']);
            if($resule == 500) echo 500;
            else echo 200;
        }else { //批量删除
            $resule = $LinkModel->del_All_Link($_POST['id']);
            if($resule == 500) echo 500;
            else echo 200;
        }
    }
    // ajax搜索友链
    public function searchLink(){
        $LinkModel = new LinkModel();
        $searchVal = $_POST['key'];
        $search = $LinkModel->sel_search($searchVal);
        $resule = (object)[];
        $resule->code = 0;
        $resule->msg = '';
        $resule->count = count($search);
        $resule->data = [];
        foreach ($search as $value) {
            array_push($resule->data,$value);
        }
        echo json_encode($resule);
    }
    #####################系统日记########################
    public function systemlogs(){
        return $this->fetch();
    }
    #####################图标管理########################
    public function icons(){
        return $this->fetch();
    }

    ################ajax上传logo函数集合##################
    public function uploadsLogo(){
        $file = request()->file('file');
        $whoLogo = input('post.whoLogo');
        if(isset($file)){
            // 移动到框架应用根目录/public/uploads 目录下  
            $path = ROOT_PATH . 'public/uploads/' . $whoLogo;
            $info = FileUploads($file, $path);
            if($info['code'] == 200){   //文件上传成功
                $SaveName = $info['data'];
                $imgpath='/uploads/'.$whoLogo.'/'.$SaveName;
                if($whoLogo == 'linkLogo'){     //友情链接logo图片上传
                    $LinkModel = new LinkModel();
                    $id = input('post.id');
                    $resule = $LinkModel->upd_Link($id,['logo'=>$imgpath]);
                }elseif ($whoLogo == 'web_Logo') {      //网站logo图片上传
                    $ConfigModel = new ConfigModel;
                    $resule = $ConfigModel->saveConfig(1,['web_logo'=>$imgpath]);
                }elseif ($whoLogo == 'author_Pic') {
                    $ConfigModel = new ConfigModel;
                    $resule = $ConfigModel->saveConfig(1,['author_pic'=>$imgpath]);
                }
                if($resule) echo json_encode(['code'=>200,'data'=>$imgpath]);
                else echo json_encode(['code'=>500,'data'=>'文件上传成功，数据更新失败！']);
            }else {
                echo json_encode(['code'=>500,'data'=>$info['msg']]);
            }
        }
    }
}