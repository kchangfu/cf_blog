<?php
namespace app\admin\controller;
use think\Controller;	//引入Controller类
use app\admin\model\Users as UsersModel;
use app\admin\model\Config as ConfigModel;
use think\Session;
use app\common; //引入公共文件

class Main extends Controller
{
    public function index(){
        $UsersModel = new UsersModel();
        $username = Session::get('username');
        $UsersInfo = $UsersModel->loginCheck($username);
        $UsersInfo['userRights'] = formatuserRights($UsersInfo['login_type']);

        $ConfigModel = new ConfigModel;
        $config = $ConfigModel->getConfig(1);
        $config['maxUpload'] = get_cfg_var ("upload_max_filesize")?get_cfg_var ("upload_max_filesize"):"不允许"; //最大上传限制
        $config['server'] = php_uname('s').' '.php_uname('r'); //服务器操作系统
        $config['dataBase'] = 'mysql'; //服务器操作系统

        $this->assign('config',$config);
        $this->assign('UsersInfo',$UsersInfo);
        return $this->fetch();
    }
}