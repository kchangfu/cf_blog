<?php
namespace app\admin\controller;
use think\Controller;	//引入Controller类
use app\common;
use think\Session;

class Index extends Controller
{
    public function index(){
        if(Session::has('username') && Session::get('type')=='admin' && Session::get('login_type')=='2'){
            return $this->fetch();
        }else{
            $this->redirect('/admin/login');
        }
    }
}