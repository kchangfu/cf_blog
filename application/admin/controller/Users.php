<?php
namespace app\admin\controller;
use think\Controller;	//引入Controller类
use think\Session;
use app\admin\model\Users as UsersModel;
use app\common;

class Users extends Controller
{
    #####################用户中心########################
    public function userList(){
        return $this->fetch();
    }
    public function GetUsers(){
        if($_GET['page'] && $_GET['limit']){
            $limit = $_GET['limit'];
            $page = $_GET['page'];
            $UsersModel = new UsersModel();
            $Users = $UsersModel->getUsers($limit, $page);
            $resule = (object)[];
            $resule->code = 0;
            $resule->msg = '';
            $resule->count = $UsersModel->getUsersCount();
            $resule->data = [];
            foreach ($Users as $value) {
                $value['login_type'] = formatuserRights($value['login_type']);
                $value['Sex'] = formatuserSex($value['Sex']);
                $value['Email'] = $value['Email'] ? $value['Email'] : '';
                array_push($resule->data,$value);
            }
            echo json_encode($resule);
        }
    }
    // 添加用户
    public function userAdd(){
        return $this->fetch();
    }
    // 用户详情
    public function userInfo(){
        return $this->fetch();
    }
    // 用户修改密码
    public function changePwd(){
        if($_POST){
            $UsersModel = new UsersModel();
            $result = $UsersModel->changePwd($_POST['username'],md5($_POST['oldPwd']),md5($_POST['newPwd']));
            if(is_int($result)) echo 'changePwd success';
            else echo 'oldPwd error';
            exit;
        }
        $username = Session::get('username');
        $Name = Session::get('Name');
        $this->assign('username',$username);
        $this->assign('name',$Name);
        return $this->fetch();
    }

    #####################用户等级########################
    public function userGrade(){
        return $this->fetch();
    }
}