<?php
namespace app\admin\model;
use think\Model;

class Config extends Model
{
    // 更新网站配置
    public function saveConfig($id,$data){
        if (empty($data) || !is_array($data)) return false;
        else {
            $resule = Config::where('id',$id)->update($data);
            return $resule;
        }
    }
    // 获取网站配置
    public function getConfig($id){
        $resule = Config::get($id)->toArray();
        return $resule;
    }
}