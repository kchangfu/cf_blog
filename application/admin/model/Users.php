<?php
namespace app\admin\model;
use think\Model;

class Users extends Model
{
    // 登录验证
    public function loginCheck($username){
        $resule = Users::where('username',$username)->select();
        if(empty($resule)) return $resule;
        else return $resule[0]->toArray();
    }

    // 最后一次登陆
    public function last_login($username, $prev_login){
        $last_login = date('Y-m-d H:i:s', time());
        $result = Users::where('username',$username)->update(['prev_login'=>$prev_login,'last_login'=>$last_login]);
        return $result;
    }

    // 修改密码
    public function changePwd($username, $oldPwd, $newPwd){
        $resule = Users::where('username',$username)->where('password',$oldPwd)->select();
        if(empty($resule)) return $resule;
        else{
            $result = Users::where('username',$resule[0]['username'])->update(['password'=>$newPwd]);
            return $result;
        }
    }

    // 获取所有用户
    public function getUsers($limit,$page){
        $Users = Users::order('id', 'asc')->limit($limit)->page($page)->select();
        return $Users;
    }
    // 获取用户总数
    public function getUsersCount(){
        $UsersCount = Users::order('id', 'asc')->select();
        return count($UsersCount);
    }
}