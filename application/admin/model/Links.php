<?php
namespace app\admin\model;
use think\Model;

class Links extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    // 获取全部友情链接
    public function getLinks($limit,$page){
        $Linklist = Links::order('rank', 'asc')->limit($limit)->page($page)->select();
        return $Linklist;
    }
    // 获取全部友情链接总数
    public function getLinksCount(){
        $LinklistCount = Links::order('rank', 'asc')->select();
        return $LinklistCount;
    }
    // 新增友情链接
    public function ins_Link($data){
        $result = Links::allowField(true)->save($data);
        if($result === false) return 500;
        else return $result;
    }
    // 更新友情链接
    public function upd_Link($id,$data){
        $result = Links::where('id',$id)->update($data);
        if($result === false) return 500;
        else return $result;
    }
    // 删除友情链接
    public function del_Link($id){
        $result = Links::where('id',$id)->delete();
        if($result === false) return 500;
        else return $result;
    }
    // 批量删除友情链接
    public function del_All_Link($ids){
        $result = Links::destroy($ids);
        if($result === false) return 500;
        else return $result;
    }
    // 搜索功能
    public function sel_search($str){
        $map['name|url|masterEmail']=array('like','%'.$str.'%');
        $result = Links::where($map)->select();
        return $result;
    }
}