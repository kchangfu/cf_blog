<?php
namespace app\index\controller;
use think\Controller;	//引入Controller类
use app\index\model\Config as ConfigModel;
use app\index\model\Links as LinkModel;

class About extends Controller
{
    public function index(){
        $ConfigModel = new ConfigModel;
        $LinkModel = new LinkModel;
        $config = $ConfigModel->getConfig(1);
        $links = $LinkModel->getLinks();
        $this->assign([
            'config'=>$config,
            'links'=>$links,
        ]);
        return $this->fetch();
    }
}