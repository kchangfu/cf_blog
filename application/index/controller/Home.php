<?php
namespace app\index\controller;
use think\Controller;	//引入Controller类
use app\index\model\Config as ConfigModel;
use app\index\model\Links as LinkModel;
use app\index\model\Users as UsersModel;
use think\Cookie;
use think\captcha\Captcha;
use app\common; //引入公共文件

class Home extends Controller
{
    // 首页页面
    public function index(){
        $ConfigModel = new ConfigModel;
        $LinkModel = new LinkModel;
        $UsersModel = new UsersModel;
        $config = $ConfigModel->getConfig(1);   //获取网站配置
        $links = $LinkModel->getLinks();    //获取友情链接
        $author = $UsersModel->getAuthor(1);    //获取站长信息
        $this->assign([
            'config'=>$config,
            'author'=>$author,
            'links'=>$links
        ]);
        return $this->fetch();
    }

    // 前台用户登陆页面
    public function login(){
        $ConfigModel = new ConfigModel;
        $config = $ConfigModel->getConfig(1);   //获取网站配置
        $this->assign([
            'config'=>$config,
        ]);
        if($_POST){
            $code = $_POST['code'];
            $username = $_POST['username'];
            $password = encryptDecrypt($_POST['password'],0);
            $captcha = new Captcha();
            $UsersModel = new UsersModel();
            $resule = $UsersModel->loginCheck($username);
            if (empty($resule)) echo 'no_user';    //用户不存在
            elseif ($resule['password'] != $password) echo 'no_pass';   //用户密码错误
            elseif (!$captcha->check($code)) echo 'Verify_code_error';  //验证码错误
            else{
                $last_login = $UsersModel->last_login($resule['username'],$resule['last_login']); //记录本次和上次登陆时间
                if(!$last_login) echo 'no_network';
                else
                Cookie::set('username',$resule['username']);
                Cookie::set('type',$resule['login_type']);
                Cookie::set('user_ico',$resule['logo']);
                echo 'yes';
            };
            die;
        }
        return $this->fetch();
    }

    // 前台用户注册页面
    public function register(){
        $ConfigModel = new ConfigModel;
        $config = $ConfigModel->getConfig(1);   //获取网站配置
        $this->assign([
            'config'=>$config,
        ]);
        if($_POST){
            $UsersModel = new UsersModel();
            $captcha = new Captcha();
            $username = $_POST['username'];
            $email = $_POST['email'];
            $code = $_POST['code'];
            if (!empty($UsersModel->checkuserName($username))){echo '400_username';die;}  //用户名重复
            if (!empty($UsersModel->checkEmail($email))){echo '400_email';die;}  //邮箱重复
            if (!$captcha->check($code)){echo '400_code';die;}  //验证码错误
            $password = encryptDecrypt($_POST['password'],0);
            $resule = $UsersModel->create_user($username,$password,$email);
            if($resule){
                Cookie::set('username',$username);
                Cookie::set('type','0');
                Cookie::set('user_ico','/static/index/images/user_ico.jpg');
                echo '200_success';
            }else{
                echo '500_error';
            }
            die;
        }
        return $this->fetch();
    }
}