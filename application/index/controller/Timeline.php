<?php
namespace app\index\controller;
use think\Controller;	//引入Controller类
use app\index\model\Config as ConfigModel;

class Timeline extends Controller
{
    public function index(){
        $config = ConfigModel::get(1)->toArray();
        $this->assign('config',$config);
        return $this->fetch();
    }
}