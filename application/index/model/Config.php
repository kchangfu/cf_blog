<?php
namespace app\index\model;
use think\Model;

class Config extends Model
{
    // 获取网站配置
    public function getConfig($id){
        $resule = Config::get($id)->toArray();
        return $resule;
    }
}