<?php
namespace app\index\model;
use think\Model;

class Links extends Model
{
    // 获取全部友情链接
    public function getLinks(){
        $Linklist = Links::all(function($query){
            $query->where('display', 1)->order('rank', 'asc');
        });
        return $Linklist;
    }
}