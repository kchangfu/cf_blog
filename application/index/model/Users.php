<?php
namespace app\index\model;
use think\Model;

class Users extends Model
{
    // 获取站长信息
    public function getAuthor($id){
        $resule = Users::get($id)->toArray();
        return $resule;
    }

    // 登录验证
    public function loginCheck($username){
        $resule = Users::where('username',$username)->select();
        if(empty($resule)) return $resule;
        else return $resule[0]->toArray();
    }

    // 最后一次登陆
    public function last_login($username, $prev_login){
        $last_login = date('Y-m-d H:i:s', time());
        $result = Users::where('username',$username)->update(['prev_login'=>$prev_login,'last_login'=>$last_login]);
        return $result;
    }

    // 注册用户
    public function create_user($username,$password,$email){
        $result = Users::create([
            'username'  =>  $username,
            'password' =>  $password,
            'Email' =>  $email,
        ]);
        return $result->id;
    }

    // 验证用户名
    public function checkuserName($username){
        $resule = Users::where('username',$username)->select();
        if(empty($resule)) return $resule;
        else return $resule[0]->toArray();
    }
    // 验证邮箱唯一
    public function checkEmail($email){
        $resule = Users::where('Email',$email)->select();
        if(empty($resule)) return $resule;
        else return $resule[0]->toArray();
    }
}